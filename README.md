# Running application
Tested on Win10 with Visual Studio Code. 
## Install requirements  
```
# Install pip
sudo apt-get install python3-pip
# Install requirements
sudo pip3 install -r requirements.txt
# Create ablemic table in db (on first run only)
flask db init
# Create migration file (if we changed model)
flask db migrate
# Apply migration to database
flask db upgrade
# Run tests
python3 tests.py
# Run app
flask run
```
## Test Description
1. test_card_create
Creates cards with parameters:
* Owner Name: К.Е.К.
* Telephone number: +79112281337 
* Item name: annihilation gun
* Serial number: ABCDEFGCK1234
2. test_get_all_equipments  
Gets all equipments contained in the database.
3. def test_get_equipment_by_id  
Creates a card with parameteres from the first test, gets its id and then tries to get that card from the database using this id.
4. test_equipment_can_be_edited  
Creates a card and attempts to change its contents in the database.
5. test_card_deletion  
Creates a card and then deletes it.
6. test_put_empty_object  
Tries to perform PUT query with empty body
7. test_post_empty_object  
Tries to perform POST query with empty body
