from flask import request, jsonify, url_for
from app.models import Equipment
from app.api import bp
from app import db
from app.api.errors import bad_request


@bp.route('/equipments/<int:id>', methods=['GET'])
def get_equipment(id):
    return jsonify(Equipment.query.get_or_404(id).to_dict())


@bp.route('/equipments', methods=['GET'])
def get_equipments():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10), 100)
    data = Equipment.to_collection_dict(Equipment.query, page, per_page, 'api.get_equipments')

    return jsonify(data)


@bp.route('/equipments', methods=['POST'])
def create_equipment():
    data = request.get_json() or {}
    if not data:
        return bad_request('Equipment card should contain something')

    equipment = Equipment()
    equipment.from_dict(data)

    db.session.add(equipment)
    db.session.commit()

    response = jsonify(equipment.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_equipment', id=equipment.id)

    return response


@bp.route('/equipments/<int:id>', methods=['PUT'])
def update_equipment(id):
    equipment = Equipment.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Equipment card should contain something')
        
    equipment.from_dict(data)
    db.session.commit()

    return jsonify(equipment.to_dict())


@bp.route('/equipments/<int:id>', methods=['DELETE'])
def remove_equipment(id):
    equipment = Equipment.query.get_or_404(id)
    db.session.delete(equipment)
    db.session.commit()

    return jsonify(equipment.to_dict())